#include "GhostHandler.h"

void GhostHandler::init(const glm::vec2 &size, const glm::vec2 &pos, const glm::vec2 &doorDir)
{
	tileSize = size;

	startPos = pos;

	for (int i = 0; i < NUM_COLORS; ++i)
	{
		isDead[i] = false;
	}

	nextSourceTimer = std::vector<float>(NUM_COLORS, 0);
	resetSourceTimer = 0.1f;

	// Initializatin of ghosts
	ghosts.push_back(std::make_shared<ScreenObject>(glm::vec2(pos.x, pos.y),  size, RED_GHOST));
	ghosts.push_back(std::make_shared<ScreenObject>(glm::vec2(pos.x, pos.y) - doorDir * size, size, GREEN_GHOST));
	ghosts.push_back(std::make_shared<ScreenObject>(glm::vec2(pos.x, pos.y) + abs(glm::vec2(doorDir.y,doorDir.x)) * size, size, YELLOW_GHOST));
	ghosts.push_back(std::make_shared<ScreenObject>(glm::vec2(pos.x, pos.y) + (abs(glm::vec2(doorDir.y,doorDir.x)) - doorDir) * size, size, BLUE_GHOST));

	// Makes ghost start by leaving spawn
	moveDir[YELLOW] = doorDir;
	goalPos[YELLOW] = ghosts[YELLOW]->position + moveDir[YELLOW] * size * 2.f;
	moveDir[BLUE] = doorDir;
	goalPos[BLUE] = ghosts[BLUE]->position + moveDir[BLUE] * size * 2.f;
	moveDir[RED] = doorDir;
	goalPos[RED] = ghosts[RED]->position + moveDir[RED] * size * 2.f;
	moveDir[GREEN] = doorDir;
	goalPos[GREEN] = ghosts[GREEN]->position + moveDir[GREEN] * size * 2.f;
}

const std::vector<std::shared_ptr<ScreenObject>>& GhostHandler::getGhosts()
{
	return ghosts;
}

void GhostHandler::die(const int& index)
{
	isDead[index] = true;
	ghosts[index]->textureName = GHOST_DEAD;
}

bool GhostHandler::isActive(const int& index)
{
	return !isDead[index];
}

void GhostHandler::update(const float& dt, const std::vector<std::vector<int>>& map, const glm::vec2& pacmanPos, const bool& activeCherry)
{
	for (size_t i = 0; i < ghosts.size(); ++i)
	{
		// Need new movement
		if (areClose(ghosts[i]->position, goalPos[i]))
		{
			// Calculate current tile and next
			int xTile = round(ghosts[i]->position.x / tileSize.x); 
			int yTile = round(ghosts[i]->position.y / tileSize.y);
			glm::vec2 end = glm::vec2(xTile, yTile) + moveDir[i];
			if (map[end.y + 1][end.x + 1] != 9)
			{
				// Pick AI
				if (isDead[i])
				{
					moveDir[i] = findTarget(dt, map, end, xTile, yTile, i, startPos);
					if (map[yTile][xTile] == 5)
					{
						isDead[i] = false;
						switch(i)
						{
							case 0: ghosts[i]->textureName = RED_GHOST;
								break;
							case 1:ghosts[i]->textureName = GREEN_GHOST;
								break;
							case 2:ghosts[i]->textureName = YELLOW_GHOST;
								break;
							case 3:ghosts[i]->textureName = BLUE_GHOST;
								break;
						}
					}
				}
				else if (activeCherry)
				{
					moveDir[i] = fleeTarget(dt, map, end, xTile, yTile, i, pacmanPos);
				}
				else
				{
					if (i == RED)
					{
						moveDir[i] = findTarget(dt, map, end, xTile, yTile, i, pacmanPos);
					}
					else 
					{
						moveDir[i] = simpleAI(dt, map, end, xTile, yTile, i);
					}
				}
			}
			else
			{
				// At the edge of the map
				moveDir[i] = -moveDir[i];
			}
			ghosts[i]->position = goalPos[i];
			goalPos[i] = ghosts[i]->position + moveDir[i] * tileSize;
		}
		ghosts[i]->position += moveDir[i]* 100.f * dt;

		updateSource(ghosts[i], i, dt, activeCherry);
	}
}

// Picks a random direction of the available ones, except opposite direction
glm::vec2 GhostHandler::simpleAI(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile, int index)
{
	std::vector<glm::vec2> temp;
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	end = glm::vec2(xTile + moveDir[index].y, yTile + moveDir[index].x);
	if(map[end.y + 1][end.x + 1] != 1 && map[end.y + 1][end.x + 1] != 6)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	end = glm::vec2(xTile - moveDir[index].y, yTile - moveDir[index].x);
	if(map[end.y + 1][end.x + 1] != 1 && map[end.y + 1][end.x + 1] != 6)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	std::uniform_int_distribution<int> distribution(0,temp.size() - 1);

	return -temp[distribution(generator)];
}

// Finds available tiles and sets movemet to the one closest to the target
glm::vec2 GhostHandler::findTarget(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile,const int& index, const glm::vec2& goal)
{
	std::vector<glm::vec2> temp;
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	end = glm::vec2(xTile + moveDir[index].y, yTile + moveDir[index].x);
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	end = glm::vec2(xTile - moveDir[index].y, yTile - moveDir[index].x);
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}

	glm::vec2 tempDir;
	tempDir = temp.front();
	for (size_t i = 1; i < temp.size(); ++i)
	{
		glm::vec2 prevVec = ghosts[index]->position + tempDir - goal;
		float prevDist = sqrt(pow(prevVec.x, 2) + pow(prevVec.y,2));

		glm::vec2 newVec = ghosts[index]->position + temp[i] - goal;
		float newDist = sqrt(pow(newVec.x, 2) + pow(newVec.y,2));

		if (newDist > prevDist)
		{
			tempDir = temp[i];
		}
	}
	return -tempDir;
}

// Finds available tiles and sets movemet to the one furthest away from the target
glm::vec2 GhostHandler::fleeTarget(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile,const int& index, const glm::vec2& goal)
{
	std::vector<glm::vec2> temp;
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	end = glm::vec2(xTile + moveDir[index].y, yTile + moveDir[index].x);
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}
	end = glm::vec2(xTile - moveDir[index].y, yTile - moveDir[index].x);
	if(map[end.y + 1][end.x + 1] != 1)
	{
		temp.push_back(glm::vec2(xTile, yTile) - end);
	}

	glm::vec2 tempDir;
	tempDir = temp.front();
	for (size_t i = 1; i < temp.size(); ++i)
	{
		glm::vec2 prevVec = ghosts[index]->position + tempDir - goal;
		float prevDist = sqrt(pow(prevVec.x, 2) + pow(prevVec.y,2));

		glm::vec2 newVec = ghosts[index]->position + temp[i] - goal;
		float newDist = sqrt(pow(newVec.x, 2) + pow(newVec.y,2));

		if (newDist < prevDist)
		{
			tempDir = temp[i];
		}
	}
	return -tempDir;
}

bool GhostHandler::areClose(const glm::vec2 &pos1, const glm::vec2 &pos2)
{
	float threshold = 0.5f;
	return pos1.x - threshold <= pos2.x && pos1.x + threshold >= pos2.x
		&& pos1.y - threshold <= pos2.y && pos1.y + threshold >= pos2.y;
}

// Animates ghosts
void GhostHandler::updateSource(const std::shared_ptr<ScreenObject>& ghost, size_t color, const float &dt, const bool& activeCherry)
{	
	if (moveDir[color].x > 0)
	{
		ghost->sourceRect.y = 0;
	}

	if (moveDir[color].x < 0)
	{
		ghost->sourceRect.y = 73 * 2;
	}

	if (moveDir[color].y > 0)
	{
		ghost->sourceRect.y = 73;
	}

	if (moveDir[color].y < 0)
	{
		ghost->sourceRect.y = 73 * 3;
	}

	if (activeCherry && !isDead[color])
	{
		ghost->sourceRect.y += 73 * 4;
	}

	nextSourceTimer[color] += dt;
	if (nextSourceTimer[color] > resetSourceTimer)
	{
		ghost->sourceRect.x += 73;
		ghost->sourceRect.x = ghost->sourceRect.x % (73 * 4);
		nextSourceTimer[color] -= resetSourceTimer;
	}
}