#pragma once

#include <vector>
#include <memory>
#include <random>

#include "ScreenObject.h"
#include "EnumScreenTexture.h"

enum ColorOrder
{
	RED,
	GREEN,
	YELLOW,
	BLUE,
	NUM_COLORS
};

class GhostHandler
{
public:
	void init(const glm::vec2 &size, const glm::vec2 &pos, const glm::vec2 &doorDir);

	void update(const float& dt, const std::vector<std::vector<int>>& map, const glm::vec2& pacmanPos, const bool& activeCherry);
	const std::vector<std::shared_ptr<ScreenObject>>& getGhosts();
	void die(const int& index);
	bool isActive(const int& index);

private:
	// Movement stuff
	glm::vec2 simpleAI(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile, int index);
	glm::vec2 findTarget(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile, const int& index, const glm::vec2& goal);
	glm::vec2 fleeTarget(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile, const int& index, const glm::vec2& goal);
	glm::vec2 moveDir[NUM_COLORS];
	glm::vec2 goalPos[NUM_COLORS];


	bool areClose(const glm::vec2 &pos1, const glm::vec2 &pos2);
	void updateSource(const std::shared_ptr<ScreenObject>& ghost, size_t color, const float &dt, const bool& activeCherry);

	std::default_random_engine generator;
	bool isDead[NUM_COLORS];
	std::vector<float> nextSourceTimer; // Time since last sourceRect switch
	float resetSourceTimer; // Threshold for when to switch sourceRect
	glm::vec2 startPos;		// Spawnpoint

	glm::vec2 tileSize;		// Size of tiles on screen

	std::vector<std::shared_ptr<ScreenObject>> ghosts;
};