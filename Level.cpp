#include "Level.h"

Level::Level(std::string filepath) : prevDir() 
{
	loadMap(filepath);
	createMap();
}

// Reads a text file containing map data
void Level::loadMap(const std::string &filepath) 
{
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);

	mapDimensions = glm::vec2(x - 2, y - 2);
	glm::vec2 screen = WindowHandler::getInstance().getScreenSize();
	tileSize = glm::vec2(screen.x/mapDimensions.x, screen.y/mapDimensions.y);

	for(int i = 0; i<y; i++)	
	{
		std::vector<int> row;
		for(int j = 0; j<x; j++) 
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createMap() 
{
	glm::vec2 pos, size;
	int rows = map.size();
	int cells = 0;
	SDL_Rect tempSource;
	glm::vec2 doorDir;
	int doorFace;

	for(int i = buffer; i < rows - buffer; i++) 
	{
		cells = map[i].size();

		for(int j = buffer; j < cells - buffer; j++) 
		{
			switch(map[i][j])
			{
				case 1:
					walls.push_back(std::make_shared<ScreenObject>(glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), tileSize, WALL));
					break;
				case 2:
					pacman = std::make_shared<Pacman>(glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), tileSize);
					goalPos = pacman->getPosition();
					startPos = goalPos;
					break;
				case 3:
					orbs.push_back(std::make_shared<ScreenObject>(glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), tileSize, ORB));
					break;
				case 4:
					scoreText.init(tileSize, glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), "Score");
					livesText.init(tileSize, glm::vec2((j - buffer) * tileSize.x, (i) * tileSize.y), "Lives");
					livesText.setValue(3);
					break;
				case 5:
					if (map[i][j+1] == 6)
						doorDir = glm::vec2(1,0);
					else if (map[i][j-1] == 6)
						doorDir = glm::vec2(-1,0);
					else if (map[i+1][j] == 6)
						doorDir = glm::vec2(0,1);
					else if (map[i-1][j] == 6)
						doorDir = glm::vec2(0,-1);
					
					ghostHandler.init(tileSize, glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), doorDir);
					break;
				case 6: 
					if (map[i][j-1] == 5)
						doorFace = 0;
					else if (map[i-1][j] == 5)
						doorFace = 1;
					else if (map[i][j+1] == 5)
						doorFace = 2;
					else if (map[i+1][j] == 5)
						doorFace = 3;

					tempSource.h = 73;
					tempSource.w = 73;
					tempSource.x = 0;
					tempSource.y = 73 * doorFace;
					walls.push_back(std::make_shared<ScreenObject>(glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), tileSize, WALL_SPECIAL, tempSource));
					break;
				case 8:
					cherries.push_back(std::make_shared<ScreenObject>(glm::vec2((j - buffer) * tileSize.x, (i - buffer) * tileSize.y), tileSize, CHERRY));
					break;
			}
		}
	}

	resetCherryCounter = 0.0f;
	cherryDuration = 10.0f;
	activeCherry = false;
}

// Death animation
void Level::die()
{
	float fpsGoal = 60.0f;
	float nextFrame = 1/fpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float endTimer = 0.0f;
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;

	// Traps game until animation is finished
	while(endTimer < 0.9f) 
	{
		clockStart = std::chrono::high_resolution_clock::now();

		if(nextFrameTimer >= nextFrame) 
		{
			pacman->die(deltaTime);
			draw();
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		nextFrameTimer += deltaTime;
		endTimer += deltaTime;
	}
}

void Level::update(const float &deltaTime, std::queue<GameEvent>& eventQueue) 
{
	GameEvent nextEvent;

	while(!eventQueue.empty()) 
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		switch (nextEvent.action)
		{
			case ActionEnum::PLAYER_MOVE_UP:
				updateBoard(deltaTime, glm::vec2(0, -1));
				break;
			case ActionEnum::PLAYER_MOVE_DOWN:
				updateBoard(deltaTime, glm::vec2(0, 1));
				break;
			case ActionEnum::PLAYER_MOVE_LEFT:
				updateBoard(deltaTime, glm::vec2(-1, 0));
				break;
			case ActionEnum::PLAYER_MOVE_RIGHT:
				updateBoard(deltaTime, glm::vec2(1, 0));
				break;	
			default:
				break;
		}
	}
}

void Level::draw() 
{
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().drawList(walls);
	WindowHandler::getInstance().drawList(scoreText.getText());
	WindowHandler::getInstance().drawList(livesText.getText());
	WindowHandler::getInstance().drawList(orbs);
	WindowHandler::getInstance().drawList(cherries);
	WindowHandler::getInstance().drawList(ghostHandler.getGhosts());
	WindowHandler::getInstance().draw(pacman);
	WindowHandler::getInstance().update();
}

void Level::updateBoard(const float &dt, const glm::vec2 &movement)
{
	resetCherryCounter += dt;
	updatePacman(dt, movement);
	ghostHandler.update(dt, map, pacman->getPosition(), activeCherry);
}

void Level::updatePacman(const float &dt, const glm::vec2 &movement)
{
	const glm::vec2 moveDir = glm::normalize(movement);
	const glm::vec2 pos = pacman->getPosition();

	if (areClose(pos, goalPos, 1.0f) || prevDir == -moveDir)
	{
		const int xTile = round(pos.x / tileSize.x) + buffer; 
		const int yTile = round(pos.y / tileSize.y) + buffer;

		const glm::vec2 end = glm::vec2(xTile, yTile) + moveDir;
		const glm::vec2 end2 = glm::vec2(xTile, yTile) + prevDir;

		// Pickup orbs
		if (map[yTile][xTile] == 3)
		{
			for ( auto &o : orbs )
			{
				if (areClose(o->getPosition(), goalPos, 1.0f))
				{
					o = orbs.back();
					scoreText.changeValue(1);
					break;
				}
			}
			
			orbs.pop_back();
			map[yTile][xTile] = 0;
		}

		// Pickup cherries
		if (map[yTile][xTile] == 8)
		{
			for ( auto &c : cherries )
			{
				if (areClose(c->getPosition(), goalPos, 1.0f))
				{
					c = cherries.back();
					break;
				}
			}
			cherries.pop_back();
			map[yTile][xTile] = 0;
			activateCherry();
		}
		
		if (map[yTile][xTile] == 9)  // Pacman is out of bounds
		{
			wrapPacman(xTile, yTile);
			pacman->move(prevDir, dt, activeCherry);
		}
		else if (map[end.y][end.x] != 1 && map[end.y][end.x] != 6) // Is new movement ok
		{
			goalPos = (end - glm::vec2(buffer, buffer)) * tileSize;
			prevDir = moveDir;
			pacman->move(moveDir, dt, activeCherry);
		}
		else if(map[end2.y][end2.x] != 1 && map[end2.y][end2.x] != 6) // Is previous movement ok
		{
			goalPos = (end2 - glm::vec2(buffer, buffer)) * tileSize;
			pacman->move(prevDir, dt, activeCherry);
		}
		else
		{
			pacman->notMoved();
		}
	}
	else // Pacman moves until at goalPos
	{
		pacman->move(prevDir, dt, activeCherry);
	}

	if (activeCherry)
	{
		if (resetCherryCounter > cherryDuration)
		{
			activeCherry = false;
		}
		pacman->cherryPowerUp(dt, resetCherryCounter, cherryDuration, activeCherry);
	}

	collideWithEnemy();
}

// Moves pacman to other side of map if moving outwards
void Level::wrapPacman(const int& xTile, const int& yTile)
{
	if (xTile == 0 && prevDir.x < 0)
	{
		pacman->setPosition(glm::vec2(tileSize.x * (mapDimensions.x), pacman->getPosition().y));
		goalPos = glm::vec2((mapDimensions.x - buffer) * tileSize.x, (yTile - buffer) * tileSize.y);
	}
	if (xTile == mapDimensions.x + buffer && prevDir.x > 0)
	{
		pacman->setPosition(glm::vec2(-tileSize.x, pacman->getPosition().y));
		goalPos = glm::vec2(0, (yTile - buffer) * tileSize.y);
	}
	if (yTile == 0 && prevDir.y < 0)
	{
		pacman->setPosition(glm::vec2(pacman->getPosition().x, tileSize.y * (mapDimensions.y)));
		goalPos = glm::vec2((xTile - buffer) * tileSize.x, (mapDimensions.y - buffer) * tileSize.y);
	}
	if (yTile == mapDimensions.y + buffer && prevDir.y > 0)
	{
		pacman->setPosition(glm::vec2(pacman->getPosition().x, -tileSize.y));
		goalPos = glm::vec2((xTile - buffer) * tileSize.x, 0);
	}
}

void Level::collideWithEnemy()
{
	for (size_t i = 0; i < ghostHandler.getGhosts().size(); ++i)
	{
		if (ghostHandler.isActive(i) && areClose(pacman->getPosition(), ghostHandler.getGhosts()[i]->getPosition(), 15.0f))
		{
			if (activeCherry)
			{
				ghostHandler.die(i);
				scoreText.changeValue(10);
			}
			else
			{
				livesText.changeValue(-1);
				if (livesText.getValue() != 0)
				{
					pacman->setPosition(startPos);
					goalPos = startPos;
				}
				break;
			}
		}
	}
}

bool Level::areClose(const glm::vec2 &pos1, const glm::vec2 &pos2, const float& threshold)
{
	return pos1.x - threshold <= pos2.x && pos1.x + threshold >= pos2.x
		&& pos1.y - threshold <= pos2.y && pos1.y + threshold >= pos2.y;
}

void Level::setup(const int& score, const int& lives)
{
	scoreText.setValue(score);
	livesText.setValue(lives);
}

bool Level::isCompleted()
{
	return orbs.empty();
}

const int& Level::getLives()
{
	return livesText.getValue();
}

const int& Level::getScore()
{
	return scoreText.getValue();
}

void Level::activateCherry()
{
	Mix_HaltMusic();
	activeCherry = true;
	resetCherryCounter = 0;
}