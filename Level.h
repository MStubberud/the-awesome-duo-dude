#pragma	once

#include <string>
#include <vector>
#include <memory>
#include <math.h>

#include <glm/glm.hpp>

#include "WindowHandler.h"
#include "InputHandler.h"
#include "ScreenObject.h"
#include "Pacman.h"
#include "WindowHandler.h"
#include "TextHandler.h"
#include "GhostHandler.h"

class Level 
{
public:
	Level(std::string filepath);

	void update(const float &deltaTime, std::queue<GameEvent>& eventQueue);
	void draw();

	const int& getLives();
	const int& getScore();

	void activateCherry();
	
	bool isCompleted();
	void setup(const int& score, const int& lives);
	void die();

private:
	// Map data
	std::vector<std::vector<int>> map;
	glm::vec2 mapDimensions;					
	std::vector<std::shared_ptr<ScreenObject>> walls;
	std::vector<std::shared_ptr<ScreenObject>> orbs;
	std::vector<std::shared_ptr<ScreenObject>> cherries;
	glm::vec2 tileSize;
	const int buffer = 1;  //edge outside of map

	// Pacman data
	std::shared_ptr<Pacman> pacman;						
	glm::vec2 prevDir; 
	glm::vec2 goalPos;
	glm::vec2 startPos;

	// Text and ghost handling
	TextHandler scoreText;
	TextHandler livesText;
	GhostHandler ghostHandler;

	// For cherry functions
	float resetCherryCounter;
	float cherryDuration;
	bool activeCherry;

	// Updates everything in the level
	void updateBoard(const float &dt, const glm::vec2 &movement);

	// Performs pacmans movement and collision detection
	void updatePacman(const float &dt, const glm::vec2 &movement);
	void wrapPacman(const int& xTile, const int& yTile);
	void collideWithEnemy();

	// Checks if pacman is at goal tile
	bool areClose(const glm::vec2 &pos1, const glm::vec2 &pos2, const float& threshold);							

	// Loads map data from file
	void loadMap(const std::string &filepath); 
	
	// This should create map
	void createMap();
};