#include "Menu.h"

Menu::Menu()
{
	glm::vec2 screen = WindowHandler::getInstance().getScreenSize();

	menuObjs = std::vector<std::shared_ptr<ScreenObject>>(NUM_OF_ITEMS, nullptr);
	buttonHighlighted = std::vector<bool>(3, false);
	muted = false;
	hoverSound = Mix_LoadWAV("Sound/tick.wav");

	SDL_Rect titleRect;
	titleRect.x = 0;
	titleRect.y = 0;
	titleRect.w = 416;
	titleRect.h = 45;

	SDL_Rect buttonRect;
	buttonRect.x = 0;
	buttonRect.y = 0;
	buttonRect.w = 200;
	buttonRect.h = 80;

	SDL_Rect soundIconRect;
	soundIconRect.x = 0;
	soundIconRect.y = 0;
	soundIconRect.w = 120;
	soundIconRect.h = 80;

	std::shared_ptr<ScreenObject> tempObj;

	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (titleRect.w / 2), screen.y / 4), 
					glm::vec2(titleRect.w, titleRect.h), 
					MENU_TITLE, titleRect);
	menuObjs[TITLE] = tempObj;

	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2), (screen.y / 4) * 2), 
					glm::vec2(buttonRect.w, buttonRect.h), 
					MENU_BUTTON, buttonRect);
	menuObjs[START] = tempObj;

	buttonRect.y = 80;
	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2), (screen.y / 4) * 2), 
					glm::vec2(buttonRect.w, buttonRect.h), 
					MENU_BUTTON, buttonRect);
	menuObjs[START_HIGHLIGHT] = tempObj;

	buttonRect.y = 320;
	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2), (screen.y / 4) * 2 + (buttonRect.h + 20)), 
					glm::vec2(buttonRect.w, buttonRect.h), 
					MENU_BUTTON, buttonRect);
	menuObjs[MUTE] = tempObj;

	buttonRect.y = 400;
	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2), (screen.y / 4) * 2 + (buttonRect.h + 20)), 
					glm::vec2(buttonRect.w, buttonRect.h), 
					MENU_BUTTON, buttonRect);
	menuObjs[MUTE_HIGHLIGHT] = tempObj;

	buttonRect.y = 160;
	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2), (screen.y / 4) * 2 + (buttonRect.h + 20) * 2), 
					glm::vec2(buttonRect.w, buttonRect.h), 
					MENU_BUTTON, buttonRect);
	menuObjs[QUIT] = tempObj;

	buttonRect.y = 240;
	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2), (screen.y / 4) * 2 + (buttonRect.h + 20) * 2), 
					glm::vec2(buttonRect.w, buttonRect.h), 
					MENU_BUTTON, buttonRect);
	menuObjs[QUIT_HIGHLIGHT] = tempObj;

	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2) + buttonRect.w + 20, (screen.y / 4) * 2 + (buttonRect.h + 20)), 
					glm::vec2(soundIconRect.w, soundIconRect.h), 
					SOUND_ICON, soundIconRect);
	menuObjs[SOUND_ON] = tempObj;

	soundIconRect.y = 80;
	tempObj = std::make_shared<ScreenObject>(
					glm::vec2((screen.x / 2) - (buttonRect.w / 2) + buttonRect.w + 20, (screen.y / 4) * 2 + (buttonRect.h + 20)), 
					glm::vec2(soundIconRect.w, soundIconRect.h), 
					SOUND_ICON, soundIconRect);
	menuObjs[SOUND_OFF] = tempObj;
}

Menu::~Menu()
{
	Mix_FreeChunk(hoverSound);
}

void Menu::update(std::queue<GameEvent>& eventQueue, bool &menuRunning, bool &gameRunning)
{
	GameEvent nextEvent;

	while(!eventQueue.empty()) 
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		switch (nextEvent.action)
		{
			case ActionEnum::MOUSEMOTION:
				handleHighligths(START);
				handleHighligths(QUIT);
				handleHighligths(MUTE);
				break;
			case ActionEnum::MOUSEBUTTONDOWN:
				if (posOnButton(InputHandler::getInstance().getMousePosition(), menuObjs[START]->getPosition()))
				{
					menuRunning = false;
				}
				else if (posOnButton(InputHandler::getInstance().getMousePosition(), menuObjs[QUIT]->getPosition()))
				{
					gameRunning = false;
				}
				else if (posOnButton(InputHandler::getInstance().getMousePosition(), menuObjs[MUTE]->getPosition()))
				{
					muted = !muted;
					if (muted)
					{
						Mix_Volume(-1, 0);
						Mix_VolumeMusic(0);
						Mix_HaltMusic();
					}
					else
					{
						Mix_Volume(-1, MIX_MAX_VOLUME);
						Mix_HaltMusic();
						Mix_VolumeMusic(MIX_MAX_VOLUME);
					}
				}
				break;
			default:
				break;
		}
	}
}

void Menu::handleHighligths(const MenuItem& item)
{
	if (posOnButton(InputHandler::getInstance().getMousePosition(), menuObjs[item]->getPosition()))
	{
		if (!buttonHighlighted[item])
		{
			Mix_PlayChannel(-1, hoverSound, 0);
		}
		buttonHighlighted[item] = true;
	}
	else
	{
		buttonHighlighted[item] = false;
	}
}

void Menu::draw()
{
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().draw(menuObjs[TITLE]);
	WindowHandler::getInstance().draw(menuObjs[buttonHighlighted[START] ? START_HIGHLIGHT : START]);
	WindowHandler::getInstance().draw(menuObjs[buttonHighlighted[QUIT] ? QUIT_HIGHLIGHT : QUIT]);
	WindowHandler::getInstance().draw(menuObjs[buttonHighlighted[MUTE] ? MUTE_HIGHLIGHT : MUTE]);
	WindowHandler::getInstance().draw(menuObjs[muted ? SOUND_OFF : SOUND_ON]);
	WindowHandler::getInstance().update();
}

bool Menu::posOnButton(const glm::vec2 &inPos, const glm::vec2 &buttonPos)
{
	return inPos.x >= buttonPos.x && inPos.x <= buttonPos.x + 200
		&& inPos.y >= buttonPos.y && inPos.y <= buttonPos.y + 80;
}