#pragma once

#include <SDL2/SDL_mixer.h>

#include "InputHandler.h"
#include "WindowHandler.h"

enum MenuItem 
{
	START,
	QUIT,
	MUTE,
	START_HIGHLIGHT,
	QUIT_HIGHLIGHT,
	MUTE_HIGHLIGHT,
	SOUND_ON,
	SOUND_OFF,
	TITLE,
	NUM_OF_ITEMS
};

class Menu
{
public:
	Menu();
	~Menu();

	void update(std::queue<GameEvent>& eventQueue,  bool &menuRunning, bool &gameRunning);
	void draw();

private:
	std::vector<std::shared_ptr<ScreenObject>> menuObjs;
	std::vector<bool> buttonHighlighted;
	bool muted;
	Mix_Chunk *hoverSound;

	void handleHighligths(const MenuItem& item);
	bool posOnButton(const glm::vec2 &inPos, const glm::vec2 &buttonPos);
};