#include "Pacman.h"

Pacman::Pacman(const glm::vec2 &pos, const glm::vec2 &sz) : ScreenObject(pos, sz, PACMAN), tileHeight(73), tileWidth(73) 
{	
	waka = Mix_LoadMUS("Sound/waka.wav");
	superwaka = Mix_LoadMUS("Sound/superwaka.wav");
	death = Mix_LoadWAV("Sound/death.wav");
	sourceRect.x = 0;
	sourceRect.y = 0;
	sourceRect.w = tileWidth;
	sourceRect.h = tileHeight;
	nextSourceTimer = 0.f;
	resetSourceTimer = 0.05f;
	dying = false;

	acceleration = 30;
	minVelocity = 100;
	velocity = 100;
}

Pacman::~Pacman()
{
	Mix_FreeMusic(waka);
	Mix_FreeMusic(superwaka);
	Mix_FreeChunk(death);
}

void Pacman::move(const glm::vec2 &moveDir, const float &dt, const bool& activeCherry)
{
	position += moveDir * velocity * dt;
	
	// Plays waka sound if it's not already playing
	if (Mix_PlayingMusic() == 0)
	{
		Mix_PlayMusic(activeCherry ? superwaka : waka, -1);
	}
	
	// Sets which animation stage pacman should be in
	nextSourceTimer += dt;
	if (nextSourceTimer > resetSourceTimer)
	{
		sourceRect.x += tileWidth;
		sourceRect.x = sourceRect.x % (tileWidth * 10);
		nextSourceTimer -= resetSourceTimer;
	}
	
	// Determines which direction pacman should face
	if (moveDir.x > 0)
	{
		sourceRect.y = 0;
	}
	if (moveDir.x < 0)
	{
		sourceRect.y = tileHeight * 2;
	}
	if (moveDir.y > 0)
	{
		sourceRect.y = tileHeight;
	}
	if (moveDir.y < 0)
	{
		sourceRect.y = tileHeight * 3;
	}
}

void Pacman::die(const float &dt)
{
	if (!dying)
	{
		dying = true;
		Mix_HaltMusic();
		Mix_PlayChannel(-1, death, 0);
		textureName = PACMAN_DEATH;
		sourceRect.x = 0;
		sourceRect.y = 0;
		nextSourceTimer += 0;
	}
	
	nextSourceTimer += dt;
	if (nextSourceTimer > (resetSourceTimer * 2))
	{
		sourceRect.x += tileWidth;
		sourceRect.x = sourceRect.x % (tileWidth * 10);
		nextSourceTimer -= (resetSourceTimer * 2);
	}
}

void Pacman::notMoved()
{
	// Stops waka sound
	Mix_HaltMusic();
}

void Pacman::setPosition(const glm::vec2 &newPos)
{
	position = newPos;
}

void Pacman::cherryPowerUp(const float& dt, const float& resetCherryCounter, const float& cherryDuration, const bool& activeCherry)
{
	float resetAccelerationTime = (cherryDuration / 2);

	textureName = PACMAN_SPECIAL;

	if (resetCherryCounter < resetAccelerationTime)
	{
		velocity += acceleration * dt;
	}
	else if (resetCherryCounter > resetAccelerationTime && velocity > minVelocity)
	{
		velocity -= acceleration * dt;
	}
	
	if (!activeCherry)
	{
		Mix_HaltMusic();
		velocity = minVelocity;
		textureName = PACMAN;
	}
}