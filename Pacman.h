#pragma once

#include <SDL2/SDL_mixer.h>

#include "ScreenObject.h"
#include "GameEvent.h"

class Pacman : public ScreenObject
{
public:
	Pacman(const glm::vec2 &pos, const glm::vec2 &sz);
	~Pacman();

	void move(const glm::vec2 &moveDir, const float &dt, const bool& activeCherry);
	void die(const float &dt);
	void setPosition(const glm::vec2 &newPos);
	void notMoved();
	void cherryPowerUp(const float& dt, const float& resetCherryCounter, const float& cherryDuration, const bool& activeCherry);

private:
	Mix_Music* waka;
	Mix_Music* superwaka;
	Mix_Chunk* death;
	
	const int tileHeight;
	const int tileWidth;

	float acceleration;
	float minVelocity;
	float velocity;
	float nextSourceTimer; // Time since last sourceRect switch
	float resetSourceTimer;// Threshold for when to switch sourceRect
	bool dying;
};