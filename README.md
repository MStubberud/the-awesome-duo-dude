# Grade: A
Like:
  The additional features.
  That you have a separate class to handle your ghosts.
Dislike:
  Some features that should probably be part of other classes are part of the Level class.

# Assignment 1

## Group creation deadline 2016/9/30 23:59:59
## Hand in deadline 2016/10/7 23:59:59

In this assignment you will be making Pac Man.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

## Required work
1. Finish the code in ```Level::createWalls()``` and ```WindowHandler::draw(ScreenObject* object)```so that walls are added to the level and can be drawn. The walls do not need to have any fancy textures. Filling an appropriate space with color will do.
2. Create a player object.
    1. Use the ```InputHandler``` to read input from **w, a, s, d** and add movement events to the event queue.
    1. Pop events from the event queue in the update function (_main.cpp_) and use them to move Pac Man around.
    1. Implement collision detection so that Pac Man cannot pass through the walls and can pick up orbs.
    1. Load the provided sprite sheet and animate Pac Man as he moves
5. Create and display objects representing orbs/crates/fruit that can be collected by Pac Man and gives him points. (Sprites/textures optional but it must be visible.) These objects should disapear as Pac Man collects them.
6. Modify the mapfile so that it includes data about where spheres that give points should be placed.
5. Create a text handling class.
    1. Use the text handling class to load the font provided.
    1. Use the font to display a score in the top left corner of the screen as Pac Man picks up orbs.
1. Update this document. (See botom.)

## Restrictions
1. No drawing to screen outside of the ```WindowHandler``` (You are free to modify it as much as you want.)
2. You **must** use the SDL2 library for the graphics.
3. Do not use SDL_TTF for the font handling.

## Suggestions for additional work
1. Add enemies (Colliding with the enemies resets Pac Man to the start position.)
    1. Give the enemies a simple AI.
2. Add functionality that let Pac Man exit on one side of the screen and enter on the other.
3. Add sound.
4. Add more levels and a way to switch between them.
5. Implement proper kerning in the font.
6. Etc

##Group comments
###Who are the members of the group?###
Øystein Ketilsønn Kjevik

Marius Stubberud

Martin Sandberg

###What did you implement and how did you do it? (Individually)###
Marius: Sound, menu, pacman & normal ghost sprites

Øystein: Ghost AI, level 2, added buffer for wrapping and implemented it in the rest of the code, door to ghost spawn sprite

Martin: Cherry effects, rest of the sprites

###What parts if any of the base code did you change and why?###
Removed globals, because they make it easier to make mistakes that are hard to track down since they can originate in any part of the code.

Moved update and draw calls to the classes that uses them, one for levels and one for menu, because it eliminates the need for excessive amounts of get-functions.

Added texture loading to windowhandler to avoid loading them multiple times.

###What was the hardest part of this assignment? (Individually)###
Øystein: Collision

Marius: Collision

Martin: Collision and adapting to new standard of coding

###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
Øystein: yes but bordering to too much

Martin: Aw yeh

Marius: Yes, I felt it was an appropriate amount of work.

###Other comments###

####Game controls
Escape button: Returns to menu (does nothing if already at menu)
WASD: Controls pacmans movement
Mouse button down: Clicks buttons in menu

#### 2016/09/24
All 3 in group worked together on one screen. We added code to ScreenObject and Level to make the level be properly drawn on the screen. Afterwards we created a Texture class to centralize all loaded textures into being held only once in memory so future classes simply fetch the texture they need rather than storing their own in memory. Tested it by using the spritesheet to draw the walls. To be fixed later.

####2016/09/28
Added texture to walls. Started using shared pointers because dealocating of memory with walls was causing problems. Made a basic pacman with a simple sprite. Created input for the player, and added simple collision.

####2016/09/30
Fixed collision. Pacman movement update. Deleted Textures files and moved the content into WindowHandler. Some modifications in inputhandler (sets actions to INACTIVE when another is set to "ACTIVE").

####2016/10/01
Had an issue where pacman would stop moving if we tried to move him in a direction he couldn't go, so we revamped the collision system to be more tile-based in order to use the previous successful move direction until the new one either succeeds or is replaced. Animated pacman and added orb pickups. Replaced the terrible pacman spritesheet with a new one, and added a texthandler class which correctly updates score based on orbs picked up.

####Rest of the time
We forgot regular updates, but continued working mainly as a group gathered around one computer. Towards the end when most of the basic stuff was handled, we started doing some things individually, but the whole project has largely been a group effort.