#include "ScreenObject.h"

ScreenObject::ScreenObject(const glm::vec2 &pos, const glm::vec2 &sz, const TextureName &texName) 
	: position(pos), size(sz), textureName(texName) 
{
	sourceRect.x = 0;
	sourceRect.y = 0;
	sourceRect.w = 73;		// Width of a single sprite
	sourceRect.h = 73;		// Height of a single sprite
}

ScreenObject::ScreenObject(const glm::vec2 &pos, const glm::vec2 &sz, const TextureName &texName, const SDL_Rect &sourceRect) 
	: position(pos), size(sz), textureName(texName) 
{
	this->sourceRect = sourceRect;
}

glm::vec2 ScreenObject::getPosition() 
{
	return position;
}

SDL_Rect ScreenObject::getSourceRect() 
{
	return sourceRect;
}

SDL_Rect ScreenObject::getDestRect() 
{
	SDL_Rect rect;

	rect.x = ceil(position.x);
	rect.y = ceil(position.y);
	rect.w = ceil(size.x);
	rect.h = ceil(size.y);

	return rect;
}

TextureName ScreenObject::getTextureName()
{
	return textureName;
}