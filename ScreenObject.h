#pragma	once

#include <SDL2/SDL.h>
#include <glm/glm.hpp>

#include "EnumScreenTexture.h"

class ScreenObject 
{
public:
	ScreenObject(const glm::vec2 &pos, const glm::vec2 &sz, const TextureName &texName);
	ScreenObject(const glm::vec2 &pos, const glm::vec2 &sz, const TextureName &texName, const SDL_Rect &sourceRect);
	virtual ~ScreenObject() {};

	glm::vec2 getPosition();
	SDL_Rect getSourceRect();
	SDL_Rect getDestRect();
	TextureName getTextureName();

 	// Circumvents classes needing child of ScreenObject
	friend class TextHandler;    
	friend class GhostHandler;

protected:
	SDL_Rect sourceRect;
	glm::vec2 position;
	glm::vec2 size;
	TextureName textureName;
};