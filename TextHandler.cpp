#include "TextHandler.h"

TextHandler::TextHandler() : tileWidth(420 / 20), tileHeight(250 / 10) 
{	
	counter = 0;
	textSpriteSheet = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz";
}

void TextHandler::init(const glm::vec2 &size, const glm::vec2 &pos, const std::string& txt)
{
	std::string textScore = txt + ":0000";

	for (size_t i = 0; i < textScore.size(); ++i)
	{
		std::shared_ptr<ScreenObject> character = std::make_shared<ScreenObject>(glm::vec2(i * size.x + pos.x, pos.y), size, FONT);				
		character->sourceRect = findSource(textScore[i]);
		text.push_back(character);
	}
}

const std::vector<std::shared_ptr<ScreenObject>> &TextHandler::getText()
{
	return text;
}

const int& TextHandler::getValue()
{
	return counter;
}

void TextHandler::changeValue(const int& num)
{
	counter += num;
	update();
}

void TextHandler::setValue(const int& num)
{
	counter = num;
	update();
}

// Searches the string for a character and returns the calculated source rect
SDL_Rect TextHandler::findSource(const char &character)
{
	SDL_Rect sourceRect;

	for (size_t i = 0; i < textSpriteSheet.size(); ++i)
	{
		if (textSpriteSheet[i] == character)
		{
			sourceRect.x = (tileWidth * i) % 420;
			sourceRect.y = ((tileWidth * i) / 420) * tileHeight;
			sourceRect.w = tileWidth;
			sourceRect.h = tileHeight;
			break;
		}
	}

	return sourceRect;
}

// Updates source rect to a new number
void TextHandler::update()
{
	std::string tempScore = std::to_string(counter);

	for (size_t i = 0; i < tempScore.size(); ++i)
	{
		size_t index = (text.size() - tempScore.size() + i);
		text[index]->sourceRect = findSource(tempScore[i]);
	}
}