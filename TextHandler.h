#pragma once

#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <string>

#include "ScreenObject.h"
#include "EnumScreenTexture.h"

class TextHandler
{
public:
	TextHandler();
	
	void init(const glm::vec2 &tileSize, const glm::vec2 &pos, const std::string& txt);
	const std::vector<std::shared_ptr<ScreenObject>> &getText();
	const int& getValue();
	void changeValue(const int& num);
	void setValue(const int& num);


private: 
	std::vector<std::shared_ptr<ScreenObject>> text;
	std::string textSpriteSheet;
	int counter;

	const int tileWidth;
	const int tileHeight;

	void update();
	SDL_Rect findSource(const char &character);
};