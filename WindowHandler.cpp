#include "WindowHandler.h"

#include <stdio.h>
#include <SDL2/SDL_image.h>

bool WindowHandler::init () 
{

	loadConfig();

	if(!initSDL()) 
	{
		return false;
	}

	loadTextures();

	return true;
}

void WindowHandler::loadConfig () 
{
	FILE* conf = fopen("windowConfig", "r");

	fscanf(conf, "Window name: %[^\n]%*c", windowName);
	fscanf(conf, "Window size: %dx%d", &windowXSize, &windowYSize);

	fclose(conf);
	conf = nullptr;
}

bool WindowHandler::initSDL () 
{
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) 
	{
		printf("Failed to init SDL\n");
		success = false;
	} 
	else 
	{
		//Set texture filtering to linear
		if(!SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}
		
		window = SDL_CreateWindow(
			windowName, 
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			windowXSize,
			windowYSize,
			SDL_WINDOW_SHOWN //replace with SDL_WINDOW_OPENGL when using OpenGL
		);
		if (window == NULL) 
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		} 
		else 
		{
			//Create renderer for window
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			if(renderer == NULL) 
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			} 
			else 
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG; 
				if(!(IMG_Init(imgFlags) & imgFlags)) 
				{ 
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError()); 
					success = false;
				}
			}

			screenSurface = SDL_GetWindowSurface(window);
		}
	}
	return success;
}

void WindowHandler::clear() 
{
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(renderer);
}

void WindowHandler::drawList(const std::vector<std::shared_ptr<ScreenObject>> &objects) 
{
	for(auto &o : (objects)) 
	{
		draw(o);
	}
}

void WindowHandler::draw(const std::shared_ptr<ScreenObject> &object) 
{
	if(object != nullptr) 
	{
		SDL_Rect source = object->getSourceRect();
		SDL_Rect destination = object->getDestRect();

		SDL_RenderCopy(renderer, textures[object->getTextureName()], &source, &destination);
	}
}

void WindowHandler::update() 
{
	SDL_RenderPresent(renderer);
}

void WindowHandler::close () 
{
	SDL_FreeSurface(screenSurface);
	screenSurface = nullptr;

	SDL_DestroyWindow(window);
	window = nullptr;

	SDL_DestroyRenderer(renderer);
	renderer = nullptr;

	IMG_Quit();
	SDL_Quit();
}

SDL_Surface* WindowHandler::getSurface() 
{
	return screenSurface;
}

glm::vec2 WindowHandler::getScreenSize() 
{
	glm::vec2 size;
	size.x = windowXSize;
	size.y = windowYSize;
	return size;
}

void WindowHandler::loadMedia(const std::string &filename)
{
	SDL_Texture* newTexture;
	//Load splash image
	SDL_Surface* destination = IMG_Load(filename.c_str());
	
	newTexture = SDL_CreateTextureFromSurface(renderer, destination);
	SDL_FreeSurface(destination);

	textures.push_back(newTexture);
}

void WindowHandler::loadTextures()
{
	loadMedia("Images/pacman.png");
	loadMedia("Images/wall.png");
	loadMedia("Images/wallSpecial.png");
	loadMedia("Images/orb.png");
	loadMedia("Images/cherry.png");
	loadMedia("Images/Arial.bmp");
	loadMedia("Images/Arial_bold.bmp");
	loadMedia("Images/blueGhost.png");
	loadMedia("Images/greenGhost.png");
	loadMedia("Images/yellowGhost.png");
	loadMedia("Images/redGhost.png");
	loadMedia("Images/ghostDead.png");
	loadMedia("Images/menuTitle.png");
	loadMedia("Images/menuButtons.png");
	loadMedia("Images/soundIcon.png");
	loadMedia("Images/pacmanPowerup.png");
	loadMedia("Images/pacmanDeath.png");
}