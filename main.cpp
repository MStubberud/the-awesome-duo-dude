#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <string>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Pacman.h"
#include "Menu.h"
#include "Level.h"


//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels(std::vector<Level>& levels) 
{
	FILE* file = fopen("levelsList", "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) 
	{
		char tempCString[51];
		fscanf(file, "%50s", tempCString);
		tempString = tempCString;
		Level level(tempString);
		levels.push_back(level);
	}
	
	fclose(file);
	file = nullptr;
	
	return &levels.front();
}

//Initializes the InputHandler and WindowHandler
//The WindowHandler intitilizes SDL
//Loads levels and returns a pointer to the first Level by calling loadLevels()
Level* init(bool *gr, bool *mr, std::vector<Level>& levels) 
{
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096);
	InputHandler::getInstance().init(gr, mr);
	if(!WindowHandler::getInstance().init()) 
	{
		*gr = false;
	}

	return loadLevels(levels);
}

//Calls cleanup code on program exit.
void close() 
{
	WindowHandler::getInstance().close();
	Mix_CloseAudio();
}

int main(int argc, char *argv[]) 
{
	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	std::vector<Level> levels;
	Level* currentLevel = nullptr;
	bool gameRunning = true;
	bool menuRunning = true;
	float fpsGoal = 60.0f;
	float nextFrame = 1/fpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;
	size_t currentLevelIndex = 0;
	
	currentLevel = init(&gameRunning, &menuRunning, levels);
	Menu menu;

	while (gameRunning && menuRunning)
	{
		InputHandler::getInstance().readInput(eventQueue);
		menu.update(eventQueue, menuRunning, gameRunning);
		menu.draw();

		if (!menuRunning)
		{
			InputHandler::getInstance().purgeInput();
		}
	
		// Main game loop
		while(gameRunning && !menuRunning) 
		{
			clockStart = std::chrono::high_resolution_clock::now();

			InputHandler::getInstance().readInput(eventQueue);
			currentLevel->update(deltaTime, eventQueue);

			if(nextFrameTimer >= nextFrame) 
			{
				currentLevel->draw();
				nextFrameTimer = 0.0f;
			}

			clockStop = std::chrono::high_resolution_clock::now();
			deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
			nextFrameTimer += deltaTime;
			
			// All orbs collected, switches to next level or menu
			if (currentLevel->isCompleted())
			{
				if (currentLevelIndex + 1 != levels.size())
				{
					int score = currentLevel->getScore();
					int lives = currentLevel->getLives();
					currentLevel = &levels[++currentLevelIndex];
					currentLevel->setup(score, lives);
					InputHandler::getInstance().purgeInput();
					Mix_HaltMusic();
				}
				else
				{
					menuRunning = true;
				}
			}
			if (currentLevel->getLives() == 0)
			{
				currentLevel->die();
				menuRunning = true;
			}

			// Reload levels
			if (menuRunning)
			{
				currentLevelIndex = 0;
				levels.clear();
				currentLevel = loadLevels(levels);
				InputHandler::getInstance().purgeInput();
			}
		}
	}

	close();
	return 0;
}