#Files to be compiled
OBJS = main.cpp WindowHandler.cpp InputHandler.cpp ScreenObject.cpp Level.cpp Pacman.cpp TextHandler.cpp GhostHandler.cpp Menu.cpp

#Compiler
CC = g++

#Compiler flags
# -Wall 		Includes warnings
# -g			Include debug information in the executable
# -std=c++14	Use c++ 14
CFLAGS = -Wall -g -std=c++14

#Linker flags
# -lSDL2		Link to the SDL2 library
# -lSDL2		Link to the SDL2_image library, needed for more advanced image file types
LFLAGS = -lSDL2 -lSDL2_image -lSDL2_mixer

#Name of our exectuable
OBJ_NAME = pacman.exe

#This is the target that compiles our executable
#Run with command: "make"
all : $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) $(LFLAGS) -o $(OBJ_NAME)

#Removes old compiled files. Usefull for running clean builds.
#Run with command: "make clean"
clean :
	rm $(OBJ_NAME)
	rm *.o

run :
	./$(OBJ_NAME)